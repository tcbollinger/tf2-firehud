"GameMenu" [$WIN32]
{
//  SKIAL NOW!!!!!!!
//
	
// I reused the existant button names, sorry, I'm lazy.  ; )
	"FindAGameButton"
	{
		"label" "Mortal Kombat!!!"
		"command" "engine connect 104.153.105.97:27016"
		"subimage" "glyph_vr"
	}
	
	"CharacterSetupButton"
	{
		"label" "Tiny Robots"
		"command" "engine connect 50.199.129.202:27015"
		"subimage" "glyph_alert"
	}
	

	
	"VRModeButton"
	{
		"label" "#MMenu_VRMode_Activate"
		"command" "engine vr_toggle"
		"subimage" "glyph_vr"
		"OnlyWhenVREnabled" "1"
	}

	// These buttons are only shown while in-game
	// and also are positioned by the .res file
	"CallVoteButton"
	{
		"label"			""
		"command"		"callvote"
		"OnlyInGame"	"1"
		"subimage" "icon_checkbox"
		"tooltip" "#MMenu_CallVote"
	}
	"MutePlayersButton"
	{
		"label"			""
		"command"		"OpenPlayerListDialog"
		"OnlyInGame"	"1"
		"subimage" "glyph_muted"
		"tooltip" "#MMenu_MutePlayers"
	}
	
	// Swap teams on SKIAL server....follow the Poomba.....
	"RequestCoachButton"
	{
		"label"			""
		"command"		"engine say /swap"
		"OnlyInGame"	"1"
		"subimage" "icon_whistle"
		"tooltip" "Swap Teams on SKIAL"
	}
	"ReportPlayerButton"
	{
		"label"			""
		"command"		"OpenReportPlayerDialog"
		"OnlyInGame"	"1"
		"subimage"		"glyph_alert"
		"tooltip"		"#MMenu_ReportPlayer"
	}
}
