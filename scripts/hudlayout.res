#base "../basefiles/hudlayout.res"

"Resource/HudLayout.res"
{
	HudPlayerStatus
	{
		"fieldName" "HudPlayerStatus"
		"visible" "1"
		"enabled" "1"
		"xpos"	"0"
		"ypos"	"0"
		"wide"	"f0"
		"tall"	"600"
	}

	HudWeaponAmmo
	{
		"fieldName" "HudWeaponAmmo"
		"visible" "1"
		"enabled" "1"
		"xpos"	"0"	[$WIN32]
		"xpos_minmode"	"r85"	[$WIN32]
		"ypos"	"0"	[$WIN32]
		"ypos_minmode"	"r36"	[$WIN32]
		"xpos"	"r131"	[$X360]
		"ypos"	"r77"	[$X360]
		"wide"	"f0"
		"tall"	"600"
	}
	
	HudItemEffectMeter
	{
		"fieldName"		"HudItemEffectMeter"
		"visible"		"1"
		"enabled"		"1"
		"xpos"			"0"	[$WIN32]
		"xpos_minmode"	"r52"	[$WIN32]
		"ypos"			"0"	[$WIN32]
		"ypos_minmode"	"r50"	[$WIN32]
		"xpos"			"r194"	[$X360]
		"ypos"			"r74"	[$X360]
		"wide"			"f0"
		"tall"			"2000"
		"zpos"			"1"
	}
	
	HudMedicCharge
	{
		"fieldName"		"HudMedicCharge"
		"visible"		"1"
		"enabled"		"1"
		"xpos"			"0"	[$WIN32]
		"xpos_minmode"	"r100"	[$WIN32]
		"ypos"			"0"	[$WIN32]
		"ypos_minmode"	"r34"	[$WIN32]
		"xpos"			"r174"	[$X360]
		"ypos"			"r90"	[$X360]
		"wide"			"f0"
		"tall"			"600"
		"MeterFG"		"White"
		"MeterBG"		"Black"
	}
	
	HudDemomanCharge
	{
		"fieldName"		"HudDemomanCharge"
		"visible"		"1"
		"enabled"		"1"
		"xpos"			"0"	[$WIN32]
		"xpos_minmode"	"r52"	[$WIN32]
		"ypos"			"0"	[$WIN32]
		"ypos_minmode"	"r40"	[$WIN32]
		"xpos"			"r112"	[$X360]
		"ypos"			"r45"	[$X360]
		"zpos"			"1"			// draw in front of ammo
		"wide"			"f0"
		"wide_minmode"	"50"
		"tall"			"600"
		"MeterFG"		"White"
		"MeterBG"		"Black"
	}	

	HudBowCharge
	{
		"fieldName"		"HudBowCharge"
		"visible"		"1"
		"enabled"		"1"
		"xpos"			"0"	[$WIN32]
		"xpos_minmode"	"r52"	[$WIN32]
		"ypos"			"0"	[$WIN32]
		"ypos_minmode"	"r40"	[$WIN32]
		"xpos"			"r112"	[$X360]
		"ypos"			"r45"	[$X360]
		"zpos"			"1"			// draw in front of ammo
		"wide"			"f0"
		"wide_minmode"	"50"
		"tall"			"600"
		"MeterFG"		"White"
		"MeterBG"		"Black"
	}

	
	CHealthAccountPanel
	{
		"fieldName"				"CHealthAccountPanel"
		"xpos"					"0"
		"xpos_minmode"			"61"
		"ypos"					"0"
		"ypos_minmode"			"r134"
		"wide"					"f0"
		"tall"  				"480"
		"visible" 				"1"
		"enabled" 				"1"
		"PaintBackgroundType"	"2"
	}
	
	CDamageAccountPanel
	{
		"fieldName"				"CDamageAccountPanel"
		"xpos"					"0"
		"ypos"					"-10"
		"wide"					"f0"
		"tall"					"480"
		"visible" 				"1"
		"enabled" 				"1"
		"PaintBackgroundType"	"2"
	}
	
	
	CMainTargetID
	{
		"fieldName" 	"CMainTargetID"
		"visible" 	"0"
		"enabled" 	"1"
		"xpos"		"c-170"
		"ypos"		"267"
		"wide"	 	"f0"
		"tall"	 	"39"
		"tall_minmode"	 	"28"
		"priority"	"40"
		"priority_lodef"	"5"

		if_vr
		{
			"ypos"		"370"
			"x_offset"	"20"
		}
	}
	
	CSpectatorTargetID
	{
		"fieldName" 	"CSpectatorTargetID"
		"visible" 	"1"
		"enabled" 	"1"
		"xpos"		"c-120"
		"ypos"		"324"
		"wide"	 	"f0"
		"tall"	 	"38"
		"tall_minmode"	 	"28"
		"priority"	"40"
		"priority_lodef" "35"
		
		"x_offset" "20"
		"y_offset" "20"
	}
	
	CSecondaryTargetID
	{
		"fieldName" 	"CSecondaryTargetID"
		"visible" 	"0"
		"enabled" 	"1"
		"xpos"		"c-170"
		"ypos"		"297"
		"wide"	 	"f0"
		"tall"	 	"39"
		"tall_minmode"	 	"28"
		"priority"	"35"

		if_vr
		{
			"ypos"		"330"
			"x_offset"	"20"
		}

	}
	
	HudDamageIndicator
	{
		"fieldName" "HudDamageIndicator"    // Remove everything besides 
		"visible" "1" 	                    //
		"enabled" "1" 	                    //	"HudDamageIndicator
		"MinimumWidth" "7"                  //	{
		"MaximumWidth" "24"                 //	}	"
		"StartRadius" "80"                  //
		"EndRadius" "80"                    // To get the default back
		"MinimumHeight" "40"                //
		"MaximumHeight" "70"                //
		"MinimumTime" "1"                   //
	}

	HudDeathNotice
	{
		"fieldName" "HudDeathNotice"
		"visible" "1"
		"enabled" "1"
		"xpos"	 "r631"	[$WIN32]
		"ypos"	 "0"	[$WIN32]
		"xpos"	 "r672"	[$X360]
		"ypos"	 "35"	[$X360]
		"wide"	 "628"
		"tall"	 "468"

		"MaxDeathNotices" "12"
		"IconScale"	  "0.35"
		"LineHeight"	  "10"
		"LineSpacing"	  "-1"
		"CornerRadius"	  "1"
		"RightJustify"	  "1"	// If 1, draw notices from the right
		
		"TextFont"		"Default"
		
		"TeamBlue"		"HUDBlueTeamSolid"
		"TeamRed"		"HUDRedTeamSolid"
		"IconColor"		"White"
		"LocalPlayerColor"	"Black"

		"BaseBackgroundColor"	"0 0 0 0"		[$WIN32]
		"LocalBackgroundColor"	"255 255 255 189"	[$WIN32]
		"BaseBackgroundColor"	"32 32 32 255"		[$X360]
		"LocalBackgroundColor"	"0 0 0 255"		[$X360]
	}

	HudTeamGoalTournament
	{
		"fieldName"				"HudTeamGoalTournament"
		"visible"				"1"
		"enabled"				"1"
		"xpos"					"c-160"
		"ypos"					"15"
		"ypos_lodef"			"75"
		"wide"					"320"
		"tall"					"245"
	}

	HudStopWatch
	{
		"fieldName"				"HudStopWatch"
		"visible"				"1"
		"enabled"				"1"
		"xpos"					"c-160"
		"ypos"					"5"
		"ypos_minmode"				"15"
		"ypos_lodef"				"75"
		"wide"					"125"
		"tall"					"55"
	}

	xHairSpread
	{
		"controlName" "CExLabel"
		"fieldName" "xHairSpread"  //uses font "resource/fonts/Crosshairs.otf"; open in windows with a font viewer to see available "letters"
		"visible" "1"
		"enabled" "1"
		"zpos" "2"
		
		"xpos" "c-101"  //less negative moves right; c-100 is default
		"ypos" "c-99"   //less negative moves down; c-100 is default
		"wide" "202"    //play with
		"tall" "198"    //play with
		
		"font" "xHairSpread"
		
		"labelText" "o"		// alphabetic position in "font" for the "letter" that appears; starts at "a-z" then "0-##" for +26 "letters"; change to suit desires; "o" is TF2 logo
		
		"textAlignment" "center"
		"fgcolor" "255 255 255 0"  //opacity 0 to hide it until player causes damage, else on screen all the time
	}
}

